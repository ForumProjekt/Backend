export class Tag {
    constructor(
        public tagID: number,
        public title: string
    ) { }
}