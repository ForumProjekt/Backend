
export class Comment {

    constructor(
        public commentID: number,
        public threadID: number,
        public userID: number,
        public creationDate: Date,
        public commentText: string,
        public upVotes: number,
        public downVotes: number
    ) {
    }
}
