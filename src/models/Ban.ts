export class Ban {
    constructor(
        public userID: string,
        public reason: string,
        public permanent?: boolean,
        public expiresAt?: Date
    ) {
    }
}