
export class Thread {

    constructor(
        public threadID?: number,
        public titel?: string,
        public threadText?: string,
        public upvotes?: number,
        public downVotes?: number,
        public views?: number,
        public creationDate?: Date,
        public lastEdit?: Date,
        public changes?: string,
        public closed?: boolean,
        public username?: string,
        public userID?: string,
        public email?: string,
        public registeredSince?: Date,
        public label?: string,
        public hexcolor?: string
    ) {
    }
}
