export class User {

    constructor(
        public uuid?: string,
        public username?: string,
        public password?: string,
        public email?: string,
        public firstname?: string,
        public lastname?: string,
        public birthdate?: string,
        public biography?: string,
        public location?: string,
        public homepage?: string,
        public level?: number,
        public rank?: number,
        public registeredSince?: string,

    ) { }
}