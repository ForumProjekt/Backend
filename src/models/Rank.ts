export class Rank {

  constructor(
    public id: number,
    public name?: string,
    public hexcolor?: string
  ) {}

}
