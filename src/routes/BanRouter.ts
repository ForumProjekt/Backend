import { Router, Request, Response, NextFunction } from 'express';
import { MySQL } from '../services/MySQL';
import { AuthenticationDAO } from '../dao/AuthenticationDAO';
import { Messages } from '../services/Messages';
import { Logger } from '../services/Logger';
import { Ban } from '../models/Ban';
import { BanDAO } from '../dao/BanDAO';

export class BanRouter {
    router: Router

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    async addBan(req: Request, res: Response, next: NextFunction) {

        let ban = new Ban(
            req.body.userID,
            req.body.reason,
            req.body.permanent,
            req.body.expiresAt
        );
        if (!ban.userID || !ban.reason) {
            res.status(405).json(Messages.banDataWrongFormat);
        }
        if (!ban.permanent) {
            ban.permanent = true;
        }
        if (ban.permanent) {
            ban.expiresAt = new Date(0);
        }

        let err = await BanDAO.addBan(ban);
        if (err.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else if (err === 'USR-ALRDY-BND') {
            res.status(418).json(Messages.banUserAlredyBanned);
        } else if (err) {
            res.status(500).json(Messages.internalServerError);
        } else {
            res.status(200).json(Messages.banAdded);
        }

    }

    async deleteBan(req: Request, res: Response, next: NextFunction) {

        let err = await BanDAO.deleteBan(req.params.userID);
        if (err.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else if (err) {
            res.status(500).json(Messages.internalServerError);
        } else {
            res.status(200).json(Messages.userDeletedSuccessful);
        }
    }

    async getBans(req: Request, res: Response, next: NextFunction) {

        let ranks = await BanDAO.getBans();

        if (!ranks.code) {
            res.status(200).json(ranks);
        } else if (ranks.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else {
            res.status(500).json(Messages.internalServerError);
        }
    }

    async getBanByUserID(req: Request, res: Response, next: NextFunction) {

        let ban = await BanDAO.getBanByUserID(req.params.userID);
        if (ban) {
            res.status(200).json(ban[0]);
        } else {
            res.status(500).json(Messages.getUserByUUIDError);
        }
    }

    async updateBan(req: Request, res: Response, next: NextFunction) {
        const ban = new Ban(
            req.body.userID,
            req.body.reason,
            req.body.permanent,
            req.body.expiresAt
        );
        if (!ban.userID || !ban.reason) {
            res.status(405).json(Messages.banDataWrongFormat);
        }
        if (!ban.permanent) {
            ban.permanent = true;
        }
        if (ban.permanent) {
            ban.expiresAt = new Date(0);
        }
        Logger.debug('[updateBan] Ban: ' + JSON.stringify(ban));

        let err = await BanDAO.updateBan(ban);

        if (err && err.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else if (err) {
            res.status(500).json(Messages.internalServerError);
            console.log(err);
        } else {
            res.status(200).json(Messages.userRankPatched);
        }
    }

    init() {

        this.router.get('/', this.getBans);
        this.router.get('/:userID', this.getBanByUserID);
        this.router.post('/', this.addBan);
        this.router.delete('/:userID', this.deleteBan);
        this.router.patch('/:userID', this.updateBan);
    }

}

// Create the HeroRouter, and export its configured Express.Router
const banRouter = new BanRouter();
banRouter.init();
const router = banRouter.router;
export default router;
