import { Router, Request, Response, NextFunction } from 'express';
import { MySQL } from '../services/MySQL';
import { Tag } from '../models/Tag';
import { TagDAO } from '../dao/TagDAO';
import { Messages } from '../services/Messages';
import { Logger } from '../services/Logger';

export class TagRouter {
    router: Router

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    async addTag(req: Request, res: Response, next: NextFunction) {

        if (!req.body.title) {
            res.status(400).json(Messages.tagAddDataError)
            return;
        }

        const getTag = await TagDAO.getTag(req.body.title);

        if (getTag[0].title === req.body.title) {
            res.status(200).json(Messages.tagAlredyExists);
        }

        let err = await TagDAO.addTag(req.body.title);
        if (err) {
            res.status(500).json(Messages.tagAddError);
        } else {
            res.status(201).json(Messages.tagAddedSuccessful);
        }
    }

    async addTagToThread(req: Request, res: Response, next: NextFunction) {

        let err = await TagDAO.addTagToThread(req.params.threadID, req.body.tagLabels);
        if (err) {
            res.status(500).json(Messages.tagAddToThreadError);
        } else {
            res.status(200).json(Messages.tagAddedToThread);
        }

    }

    async deleteTag(req: Request, res: Response, next: NextFunction) {

        let err = await TagDAO.deleteTagFromThread(req.params.threadID, req.body.tag);
        if (err) {
            res.status(500).json(Messages.tagDeleteError);
        } else {
            res.status(200).json(Messages.tagDeleted);
        }
    }

    init() {

        this.router.post('/', this.addTag);
        this.router.post('/:threadID', this.addTagToThread);
        this.router.delete('/:tagID', this.deleteTag);
    }

}

// Create the HeroRouter, and export its configured Express.Router
const tagRouter = new TagRouter();
tagRouter.init();

export default tagRouter.router;
