import { Router, Request, Response, NextFunction } from 'express';
import { UserDAO } from '../dao/UserDAO'
import { MySQL } from '../services/MySQL';
import { User } from '../models/User';
import { AuthenticationDAO } from '../dao/AuthenticationDAO';
import { Messages } from '../services/Messages';
import { Rank } from '../models/Rank';
import { Logger } from '../services/Logger';
import { RankDAO } from '../dao/RankDAO';

export class AdminCPRouter {
    router: Router

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    async addRank(req: Request, res: Response, next: NextFunction) {

        let rank = new Rank(
            req.body.id,
            req.body.name,
            req.body.hexcolor);

        if (!rank.id || !rank.name || !rank.hexcolor) {
            res.status(405).json(Messages.rankCreationWrongFormat);
        }

        let err = await RankDAO.addRank(rank);
        if (err.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else if (err) {
            res.status(500).json(Messages.internalServerError);
        } else {
            res.status(201).json(Messages.rankAdded);
        }
    }

    async deleteRank(req: Request, res: Response, next: NextFunction) {

        let err = await RankDAO.deleteRank(req.params.id);
        if (err.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else if (err) {
            res.status(500).json(Messages.internalServerError);
        } else {
            res.status(200).json(Messages.userDeletedSuccessful);
        }
    }

    async getRanks(req: Request, res: Response, next: NextFunction) {

        let ranks = await RankDAO.getRanks();

        if (!ranks.code) {
            res.status(200).json(ranks);
        } else if (ranks.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else {
            res.status(500).json(Messages.internalServerError);
        }
    }

    async getRankById(req: Request, res: Response, next: NextFunction) {

        let rank = await RankDAO.getRankByID(req.params.id);
        if (rank) {
            res.status(200).json(rank[0]);
        } else {
            res.status(500).json(Messages.getUserByUUIDError);
        }
    }

    async patchRank(req: Request, res: Response, next: NextFunction) {
        const rank = new Rank(req.body.id, req.body.label, req.body.hexcolor);
        Logger.debug('[patchRank] Rank: ' + JSON.stringify(rank));

        let err = await RankDAO.patchRank(rank);

        if (err && err.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else if (err) {
            res.status(500).json(Messages.internalServerError);
            console.log(err);
        } else {
            res.status(200).json(Messages.userRankPatched);
        }
    }

    async patchUserRank(req: Request, res: Response, next: NextFunction) {

        const rank = new Rank(req.body.id);
        const user = new User(req.params.uuid);
        Logger.debug('[patchUserRank] Rank:' + JSON.stringify(rank) + ', User:' + JSON.stringify(user));

        let err = await UserDAO.patchUserRank(rank, user);

        if (err && err.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else if (err) {
            res.status(500).json(Messages.internalServerError);
            console.log(err);
        } else {
            res.status(200).json(Messages.userRankPatched);
        }

    }

    init() {

        this.router.get('/', this.getRanks);
        this.router.get('/:id', this.getRankById);
        this.router.post('/', this.addRank);
        this.router.delete('/:id', this.deleteRank);
        this.router.patch('/:id', this.patchRank);
        this.router.patch('/user/:id', this.patchUserRank);
    }

}

// Create the HeroRouter, and export its configured Express.Router
const acpRouter = new AdminCPRouter();
acpRouter.init();
const router = acpRouter.router;
export default router;
