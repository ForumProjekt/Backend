import { Router, Request, Response, NextFunction } from 'express';
import { ThreadDAO } from '../dao/ThreadDAO'
import { CommentDAO } from '../dao/CommentDAO'
import { MySQL } from '../services/MySQL';
import { Thread } from '../models/Thread';
import { Messages } from '../services/Messages';
import { Comment } from '../models/Comment';

export class ThreadRouter {
    router: Router

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    async addThread(req: Request, res: Response, next: NextFunction) {

        let thread = new Thread(
            0,
            req.body.titel,
            req.body.threadText,
            req.body.upvotes,
            req.body.downVotes,
            req.body.views,
            req.body.creationDate,
            req.body.lastEdit,
            req.body.changes,
            req.body.username,
            req.body.userID,
            req.body.email,
            req.body.registeredSince,
            req.body.label,
            req.body.hexcolor
        );

        if (!thread.titel || !thread.threadText || !thread.userID) {
            res.status(400).json(Messages.threadCreateWrongFormat);
            return;
        }

        let err = await ThreadDAO.addThread(thread, req.body.threadTags);
        if (err[0].threadID) {
            // Send Success with ThreadID for adding Tags later
            res.status(201).json({ 'statusCode': 201, 'message': 'Thread created', 'threadID': err[0].threadID});
        } else if (err) {
            res.status(500).json(Messages.threadCreationError);
        } else {
            res.status(201).json(Messages.threadCreated);
        }
    }

    async deleteThread(req: Request, res: Response, next: NextFunction) {

        let err = await ThreadDAO.deleteThread(req.params.threadID);
        if (err) {
            res.status(500).send('Serverfehler beim Löschen');
        } else {
            res.status(200).send('Success');
        }
    }

    async getThreads(req: Request, res: Response, next: NextFunction) {

        let threads = await ThreadDAO.getThreads();
        if (threads) {
            res.status(200).send(threads);
        } else {
            res.status(500).send('Serverfehler beim Löschen');
        }
    }

    async getThreadsbyID(req: Request, res: Response, next: NextFunction) {

        const thread = await ThreadDAO.getThreadsbyID(req.params.threadID);
        if (thread) {
            res.status(200).send(JSON.stringify(thread));
        } else {
            res.status(500).send('500 - Internal Server error while getting single Thread');
        }

    }

    async getCommentsByThread(req: Request, res: Response, next: NextFunction) {

        let comments = await CommentDAO.getThreadComment(req.params.threadID);
        if (comments) {
            res.status(200).send(JSON.stringify(comments[0]));
        } else {
            res.status(500).send('Serverfehler beim Löschen');
        }
    }


    async patchThread(req: Request, res: Response, next: NextFunction) {

        let err = await ThreadDAO.updateThread(req.params.threadID, req.body.titel, req.body.threadText, req.body.threadTags);
        if (err) {
            res.status(500).send('Serverfehler beim Verändern');
        } else {
            res.status(200).send('Success');
        }
    }

    init() {

        this.router.post('/', this.addThread);
        this.router.delete('/:threadID', this.deleteThread);
        this.router.get('/', this.getThreads);
        this.router.get('/:threadID', this.getThreadsbyID);
        // this.router.get('/:threadID', this.getCommentsByThread);
        this.router.patch('/:threadID', this.patchThread);
    }

}

// Create the HeroRouter, and export its configured Express.Router
const threadRouter = new ThreadRouter();
threadRouter.init();
const router = threadRouter.router;
export default router;
