import { Router, Request, Response, NextFunction } from 'express';
import { MySQL } from '../services/MySQL';
import { Messages } from '../services/Messages';
const os = require('os-utils');

export class SystemRouter {
    router: Router

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    async getFreeRam(req: Request, res: Response, next: NextFunction) {

        let ram = os.freemem();
        if (ram) {
            res.status(200).send(ram + '');
        } else {
            res.status(500).json(Messages.internalServerError);
        }
    }

    async getUsedRam(req: Request, res: Response, next: NextFunction) {

        const maxRam = os.totalmem();
        const freeRam = os.freemem();
        const usedRam = maxRam - freeRam;
        if (usedRam) {
            res.status(200).send(usedRam + '');
        } else {
            res.status(500).json(Messages.internalServerError);
        }
    }

    async getMaxRam(req: Request, res: Response, next: NextFunction) {

        const maxRam = os.totalmem();
        if (maxRam) {
            res.status(200).send(maxRam + '');
        } else {
            res.status(500).json(Messages.internalServerError);
        }
    }

    async getUptime(req: Request, res: Response, next: NextFunction) {

        const uptime = os.sysUptime();
        if (uptime) {
            res.status(200).send(uptime + '');
        } else {
            res.status(500).json(Messages.internalServerError);
        }
    }

    async getHostOS(req: Request, res: Response, next: NextFunction) {

        const platform = os.platform().toString();
        if (platform) {
            res.status(200).send(platform + '');
        } else {
            res.status(500).json(Messages.internalServerError);
        }
    }

    async getCPUCount(req: Request, res: Response, next: NextFunction) {

        const cpu = os.cpuCount();
        if (cpu) {
            res.status(200).send(cpu + '');
        } else {
            res.status(500).json(Messages.internalServerError);
        }
    }

    async getAvgLoad(req: Request, res: Response, next: NextFunction) {

        let avg = {
            'avg1': os.loadavg(1),
            'avg10': os.loadavg(10),
            'avg15': os.loadavg(15)
        }
        if (avg) {
            res.status(200).send(avg);
        } else {
            res.status(500).json(Messages.internalServerError);
        }
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/freeRam', this.getFreeRam);
        this.router.get('/usedRam', this.getUsedRam);
        this.router.get('/maxRam', this.getMaxRam);
        this.router.get('/uptime', this.getUptime);
        this.router.get('/hostOS', this.getHostOS);
        this.router.get('/cpus', this.getCPUCount);
        this.router.get('/avgLoad', this.getAvgLoad);
    }

}

// Create the SystemRouter, and export its configured Express.Router
const systemRoutes = new SystemRouter();
systemRoutes.init();
const router = systemRoutes.router;
export default router;
