import { Router, Request, Response, NextFunction } from 'express';
import { UserDAO } from '../dao/UserDAO'
import { MySQL } from '../services/MySQL';
import { User } from '../models/User';
import { AuthenticationDAO } from '../dao/AuthenticationDAO';
import { Messages } from '../services/Messages';
import { Rank } from '../models/Rank';
import { Logger } from '../services/Logger';

export class UserRouter {
    router: Router

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    async addUser(req: Request, res: Response, next: NextFunction) {

        let user = new User(
            req.body.uuid,
            req.body.username,
            '',
            req.body.email,
            req.body.firstname,
            req.body.lastname,
            req.body.birthdate,
            req.body.biography,
            req.body.location,
            req.body.homepage,
            req.body.level,
            req.body.rank)

        if (!user.uuid || !user.username || !user.email || !user.firstname || !user.lastname || !user.birthdate || !user.biography || !user.location || !user.homepage || !user.level || !user.rank) {
            res.status(405).json(Messages.userRegisterWrongFormat);
        }

        let err =
            await AuthenticationDAO.register(user);
        if (err) {
            res.status(500).json(Messages.internalServerError);
        } else {
            res.status(200).json(Messages.userRegistered);
        }
    }

    async deleteUser(req: Request, res: Response, next: NextFunction) {

        let err = await UserDAO.deleteUser(req.params.uuid);
        if (err.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else if (err) {
            res.status(500).json(Messages.internalServerError);
        } else {
            res.status(200).json(Messages.userDeletedSuccessful);
        }
    }

    async getUsers(req: Request, res: Response, next: NextFunction) {

        let users = await UserDAO.getUsers();

        if (!users.code) {
            res.status(200).json(users);
        } else if (users.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else {
            res.status(500).json(Messages.internalServerError);
        }
    }

    async getUserByID(req: Request, res: Response, next: NextFunction) {

        let user = await UserDAO.getUserByID(req.params.uuid);
        if (user) {
            res.status(200).json(user[0]);
        } else {
            res.status(500).json(Messages.getUserByUUIDError);
        }
    }

    async patchUser(req: Request, res: Response, next: NextFunction) {

        const user = new User(
            req.body.uuid,
            req.body.username,
            req.body.password,
            req.body.email,
            req.body.firstname,
            req.body.lastname,
            req.body.birthdate,
            req.body.biography,
            req.body.location,
            req.body.homepage,
            req.body.level,
            req.body.rank
        );
        Logger.debug('[patchUser] user: ' + JSON.stringify(user));

        let err = await UserDAO.updateUser(user);
        if (err) {
            res.status(500).json(Messages.userPatchError);
        } else {
            res.status(200).json(Messages.userPatchSuccessful);
        }
    }

    async patchUserRank(req: Request, res: Response, next: NextFunction) {

        const rank = new Rank(req.body.id);
        const user = new User(req.params.uuid);
        Logger.debug('[patchUserRank] Rank:' + JSON.stringify(rank) + ', User:' + JSON.stringify(user));

        let err = await UserDAO.patchUserRank(rank, user);

        if (err && err.code === 'ECONNREFUSED') {
            res.status(500).json(Messages.dbConnectionFailed);
        } else if (err) {
            res.status(500).json(Messages.internalServerError);
            console.log(err);
        } else {
            res.status(200).json(Messages.userRankPatched);
        }

    }

    init() {

        this.router.post('/', this.addUser);
        this.router.delete('/:uuid', this.deleteUser);
        this.router.get('/', this.getUsers);
        this.router.get('/:uuid', this.getUserByID);
        this.router.patch('/:uuid', this.patchUser);
        this.router.patch('/updateRank/:uuid', this.patchUserRank)
    }

}

// Create the HeroRouter, and export its configured Express.Router
const userRouter = new UserRouter();
userRouter.init();
const router = userRouter.router;
export default router;
