import { Router, Request, Response, NextFunction } from 'express';
import { MySQL } from '../services/MySQL';
import { Notification } from '../models/Notification';
import { NotificationDAO } from '../dao/NotificationDAO';


export class NotificationRouter {
    router: Router

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    async addNotification(req: Request, res: Response, next: NextFunction) {
        console.log(JSON.stringify(req.body.notificationText));

        let notification = new Notification(0, req.body.notificationText)

        let err = await NotificationDAO.addNotification(notification);
        if (err) {
            res.status(500).send('Serverfehler beim hinzufügen');
        } else {
            res.status(200).send('Success');
        }
    }

    async deleteNotification(req: Request, res: Response, next: NextFunction) {

        let err = await NotificationDAO.deleteNotification(req.params.notificationId);
        if (err) {
            res.status(500).send('Serverfehler beim Löschen');
        } else {
            res.status(200).send('Success');
        }
    }

    async getAllNotifications(req: Request, res: Response, next: NextFunction) {

        let notifications = await NotificationDAO.getNotifications();
        if (notifications) {
            res.status(200).send(JSON.stringify(notifications));
        } else {
            res.status(500).send('Serverfehler beim Abrufen');
        }
    }

    init() {

        this.router.post('/', this.addNotification);
        this.router.delete('/:alertID', this.deleteNotification);
        this.router.get('/', this.getAllNotifications);
    }

}

const notificationRouter = new NotificationRouter();
notificationRouter.init();

export default notificationRouter.router;
