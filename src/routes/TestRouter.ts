import { Router, Request, Response, NextFunction } from 'express';
import { MySQL } from '../services/MySQL';
const Heroes = require('../data');

export class TestRouter {
    router: Router

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    public async test(req: Request, res: Response, next: NextFunction) {
        try {
            let chatMessages = await MySQL.dBClient.query(`SELECT * FROM chat`);
            console.log(chatMessages);
            res.send(chatMessages);
        } catch (error) {
            console.log("Fehler: " + error);
            res.status(500).send("500 - Internal Server Error");
        }

    }
    /**
     * GET all Heroes.
     */
    public getAll(req: Request, res: Response, next: NextFunction) {
        res.send(Heroes);
    }

    /**
     * GET one hero by id
     */
    public getOne(req: Request, res: Response, next: NextFunction) {
        let query = parseInt(req.params.id);
        let hero = Heroes.find(hero => hero.id === query);
        if (hero) {
            res.status(200)
                .send({
                    message: 'Success',
                    status: res.status,
                    hero
                });
        }
        else {
            res.status(404)
                .send({
                    message: 'No hero found with the given id.',
                    status: res.status
                });
        }
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/', this.test);
        this.router.get('/:id', this.getOne);
    }

}

// Create the HeroRouter, and export its configured Express.Router
const testRoutes = new TestRouter();
testRoutes.init();
const router = testRoutes.router;
export default router;
