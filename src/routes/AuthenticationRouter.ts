import { Router, Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import * as fs from 'fs';
import { MySQL } from '../services/MySQL';
import { AuthenticationDAO } from '../dao/AuthenticationDAO';
import { Logger } from '../services/Logger';
import { User } from '../models/User';
import { UserDAO } from '../dao/UserDAO';
import { Messages } from '../services/Messages';

export class AuthenticationRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.init();
  }

  async login(req: Request, res: Response, next: NextFunction) {
    let _user = new User('', req.body.username, req.body.password);
    let _login;
    let _fullUser = new User();

    try {
      // fetch UUID for username from DB
      _user = await AuthenticationDAO.getUUIDFromUser(_user);
      _fullUser = await UserDAO.getUserByID(_user.uuid);
      // Bekommt warum auch immer noch ein Array aus DB
      _fullUser = _fullUser[0];
      // Logger.debug('login - UUID from user: ' + _user.uuid);
    } catch (error) {
      if (error.code === 'ECONNREFUSED') {
        Logger.error('login - Can\'t connect to Database!');
        res.status(500).json(Messages.dbConnectionFailed);
      } else {
        Logger.error('login - Can\'t fetch UUID for given username: ' + error);
      }
    }

    _login = await AuthenticationDAO.validateLoginData(_user);
    Logger.debug('login - _login status: ' + _login);

    if (_login) {
      Logger.debug('login - authentication data check passed');
      Logger.debug('login - fullUser Object: ' + JSON.stringify(_fullUser));

      // get private key from file
      var _privateKey = fs.readFileSync('./src/keys/pointless-JWT-private.key');
      // console.log(JSON.stringify(_fullUser));
      // console.log(_fullUser[0].rank);
      // generate jwt with private key + uuid asynchronously and send it back
      jwt.sign({ subject: _fullUser.uuid, rank: _fullUser.rank }, _privateKey, { algorithm: 'RS256' }, function (err, token) {
        res.status(200).json({
          idToken: token,
          uuid: _user.uuid,
          expiresIn: 21600
        });
        Logger.info(_user.uuid + ' logged in successful!');
      });

    } else {
      Logger.info('authentication data check failed for uuid ' + _user.uuid);
      // send status 401 Unauthorized
      res.status(401).send('401 - Please check your login data');
    }

  }

  async updatePassword(req: Request, res: Response, next: NextFunction) {
    let _user = new User('', req.body.username, req.body.newPassword);

    try {
      _user = await AuthenticationDAO.getUUIDFromUser(_user);
    } catch (error) {
      Logger.error('Can\'t fetch UUID for given username: ' + error);
    }

    if (_user.uuid) {
      await AuthenticationDAO.savePassword(_user);
      res.status(200).send('200 - Password update succeeded!');
    } else {
      Logger.error('updatePassword - UUID ' + _user.uuid + ' not found!');
      res.status(500).send('500 - UUID not found!');
    }

  }

  async registerNewUser(req: Request, res: Response, next: NextFunction) {
    const user = new User('', req.body.username, req.body.password, req.body.email, req.body.firstname, req.body.lastname, req.body.birthdate, req.body.bio, req.body.location, req.body.homepage)

    if (!user.uuid || !user.username || !user.email || !user.firstname || !user.lastname || !user.birthdate || !user.biography || !user.location || !user.homepage || !user.level || !user.rank) {
      res.status(400).json(Messages.userRegisterWrongFormat);
    }

    const _regStatus = await AuthenticationDAO.register(user);

    if (_regStatus) {
      Logger.info('Registered new User with UUID \'' + user.uuid + '\'');
      res.status(200).json(Messages.userRegistered);
    } else {
      res.status(500).send('500 - an error occurred while processing your request');
    }
  }

  init() {
    this.router.post('/login', this.login);
    this.router.patch('/updatePassword', this.updatePassword);
    this.router.post('/register', this.registerNewUser);
  }

}
const authRoutes = new AuthenticationRouter();
authRoutes.init();
const router = authRoutes.router;
export default router;