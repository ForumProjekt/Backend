import { Router, Request, Response, NextFunction } from 'express';
import { CommentDAO } from '../dao/CommentDAO'
import { MySQL } from '../services/MySQL';
import { Comment } from '../models/Comment';
const Heroes = require('../data');

export class CommentRouter {
    router: Router

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    async addComment(req: Request, res: Response, next: NextFunction) {

        let comment = new Comment(req.body.commentID,
            req.body.threadID,
            req.body.userID,
            req.body.creationDate,
            req.body.commentText,
            req.body.upVotes,
            req.body.downVotes)

        if (!comment.commentID || !comment.threadID) {
            res.status(405).send('Daten wurden nicht richig angegeben');
            return;
        }

        let err = await CommentDAO.addComment(comment);
        if (err) {
            res.status(500).send('Serverfehler beim hinzufügen');
        } else {
            res.status(200).send('Success');
        }
    }

    async deleteComment(req: Request, res: Response, next: NextFunction) {

        let err = await CommentDAO.deleteComment(req.params.commentID);
        if (err) {
            res.status(500).send('Serverfehler beim Löschen');
        } else {
            res.status(200).send('Success');
        }
    }

    async updateComment(req: Request, res: Response, next: NextFunction) {

        let err = await CommentDAO.updateComment(req.params.commentID);
        if (err) {
            res.status(500).send('Serverfehler beim Ändern');
        } else {
            res.status(200).send('Success');
        }
    }

    async getAllComments(req: Request, res: Response, next: NextFunction) {

        let comments = await CommentDAO.getAllComments();
        if (comments) {
            res.status(200).send(JSON.stringify(comments));
        } else {
            res.status(500).send('Serverfehler beim lesen');
        }
    }

    async getThreadComments(req: Request, res: Response, next: NextFunction) {

        let comments = await CommentDAO.getThreadComment(req.params.threadID);
        if (comments) {
            res.status(200).send(JSON.stringify(comments));
        } else {
            res.status(500).send('Serverfehler beim lesen');
        }
    }

    init() {

        this.router.post('/', this.addComment);
        this.router.delete('/:commentID', this.deleteComment);
        this.router.patch('/:commentID', this.updateComment);
        this.router.get('/', this.getAllComments);
        this.router.get('/:threadID', this.getThreadComments);
    }

}

// Create the HeroRouter, and export its configured Express.Router
const commentRouter = new CommentRouter();
commentRouter.init();
const router = commentRouter.router;
export default router;
