const config = require('../backendConfig.json');

export class Logger {

    static debug(message: string) {
        if (config.loglevel >= 3) {
            console.log("[DEBUG] " + message);
        }
    }

    static warning(message: string) {
        if (config.loglevel >= 2) {
            console.log("[WARNING] " + message);
        }
    }

    static error(message: string) {
        if (config.loglevel >= 1) {
            console.log("[ERROR] " + message);
        }
    }

    static sql(message: string) {
        if (config.loglevel >= 1) {
            console.log("[ERROR] [SQL] " + message);
        }
    }

    static info(message: string) {
        console.log("[INFO] " + message);
    }

}