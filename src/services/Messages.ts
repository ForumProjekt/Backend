export class Messages {

    // General
    public static internalServerError = { 'statusCode': 500, 'message': 'Internal server error' };
    public static routeNotFound = { 'statusCode': 404, 'message': 'Route not found', 'additionalMessage': 'visit our dev docs for help: https://docs.pointless.ga' };
    public static dbConnectionFailed = { 'statusCode': 500, 'message': 'Internal server error', 'additionalMessage': 'Couldn\'t connect to Database', 'errorType': 'databaseConnection' };

    // User
    public static userPatchSuccessful = { 'statusCode': 200, 'message': 'Patch successful' };
    public static userPatchError = { 'statusCode': 500, 'message': 'Internal server error', 'additionalMessage': 'Couldn\'t patch User' };
    public static getUserByUUIDError = { 'statusCode': 500, 'message': 'Internal server error', 'additionalMessage': 'UUID not found' };
    public static userDeletedSuccessful = { 'statusCode': 200, 'message': 'User successfuly deleted' };
    public static userRegistered = { 'statusCode': 200, 'message': 'Successfuly registered new user' };
    public static userRegisterWrongFormat = { 'statusCode': 400, 'message': 'Data processing error', 'additionalMessage': 'User data was given in wrong format or parts missing' };
    public static userRankPatched = { 'statusCode': 200, 'message': 'Successfuly patched user rank' };

    // Tag
    public static tagAddedSuccessful = { 'statusCode': 201, 'message': 'Tag created' };
    public static tagAlredyExists = { 'statusCode': 200, 'message': 'Tag alredy exists' };
    public static tagAddError = { 'statusCode': 500, 'message': 'Internal server error', 'additionalMessage': 'Couldn\'t create Tag' };
    public static tagAddDataError = { 'statusCode': 400, 'message': 'Data processing error', 'additionalMessage': 'Data was given in wrong format' };
    public static tagAddToThreadError = { 'statusCode': 500, 'message': 'Internal server error', 'additionalMessage': 'Couldn\'t add Tag to Thread' };
    public static tagAddedToThread = { 'statusCode': 201, 'message': 'Tag added successfuly to Thread' };
    public static tagDeleteError = { 'statusCode': 500, 'message': 'Internal server error', 'additionalMessage': 'Couldn\'t delete Tag' };
    public static tagDeleted = { 'statusCode': 200, 'message': 'Tag successful deleted' };

    // Thread
    public static threadCreateWrongFormat = { 'statusCode': 400, 'message': 'Data processing error', 'additionalMessage': 'Thread data was given in wrong format or parts missing' };
    public static threadCreated = { 'statusCode': 201, 'message': 'Thread created' };
    public static threadCreationError = { 'statusCode': 500, 'message': 'Internal server error', 'additionalMessage': 'Couldn\'t create Thread' };

    // AdminCP
    public static rankCreationWrongFormat = { 'statusCode': 400, 'message': 'Data processing error', 'additionalMessage': 'Rank data was given in wrong format or parts missing' };
    public static rankAdded = { 'statusCode': 201, 'message': 'New rank created' };

    // Ban
    public static banDataWrongFormat = { 'statusCode': 405, 'message': 'Data processing error', 'additinoalMessage': 'Ban data was given in wrong formats or parts missing' };
    public static banAdded = { 'statusCode': 200, 'message': 'User successfuly banned' };
    public static banUserAlredyBanned = { 'statusCode': 418, message: 'User is alredy banned!', additionalMessage: 'I\'m a teapot :P' };

}