import MySQLClient = require("mysql-pro");
const config = require('../backendConfig.json');

export class MySQL {

    // DB Client
    static dBClient = new MySQLClient({
        mysql: {
            host: config.mysql.host,
            port: config.mysql.port,
            database: config.mysql.database,
            user: config.mysql.username,
            password: config.mysql.password
        }
    });

    // Usage:
    // await MySQL.dbClient.query(``);

    // Catch error:
    // await MySQL.dBClient.query(``).then(function (error)) {
    //    console.log(error);
    // }

}