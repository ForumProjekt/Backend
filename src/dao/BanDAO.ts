import { MySQL } from '../services/MySQL';
import { User } from '../models/User';
import { AuthenticationDAO } from './AuthenticationDAO';
import { Logger } from '../services/Logger';
import { Ban } from '../models/Ban';

export class BanDAO {

    static async addBan(ban: Ban) {
        const alredyBanned = await this.getBanByUserID(ban.userID);

        if (alredyBanned[0].id) {
            Logger.error('[addBan] User alredy banned with BanID ' + alredyBanned[0].id);
            return 'USR-ALRDY-BND';
        }
        if (ban.permanent) {
            try {
                await MySQL.dBClient.query(`INSERT INTO bans (userID, reason, permanent) VALUES (?, ?, true)`, [ban.userID, ban.reason]);
            } catch (e) {
                Logger.debug('[addBan] ' + e);
                return e;
            }
        } else {
            try {
                await MySQL.dBClient.query(`INSERT INTO bans (userID, reason, permanent, expiresAt) VALUES (?, ?, false, ?)`, [ban.userID, ban.reason, ban.expiresAt]);
            } catch (e) {
                Logger.debug('[addBan] ' + e);
                return e;
            }
        }

    }

    static async deleteBan(userID: string) {

        try {
            await MySQL.dBClient.query(`DELETE FROM bans WHERE userID=?`, [userID]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getBans() {

        try {
            return await MySQL.dBClient.query(`SELECT id,userID,reason,permanent,expiresAt,users.username FROM bans JOIN users ON userID=users.uuid`);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getBanByUserID(userID: string) {

        try {
            return await MySQL.dBClient.query(`SELECT id,userID,reason,permanent,expiresAt,users.username FROM bans JOIN users ON userID=users.uuid WHERE userID=?`, [userID]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async updateBan(ban: Ban) {

        try {
            if (ban.permanent) {
                await MySQL.dBClient.query(`UPDATE bans SET reason = ?, permanent = true WHERE userID = ?`, [ban.reason, ban.userID]);
            } else {
                await MySQL.dBClient.query(`UPDATE bans SET reason = ?, permanent = ?, expiresAt = ? WHERE userID = ?`, [ban.reason, ban.permanent, ban.expiresAt, ban.userID]);
            }
        } catch (e) {
            console.log(e);
            return e;
        }
    }

}