import { MySQL } from '../services/MySQL';
import { Comment } from '../models/Comment';

export class CommentDAO {
    static async addComment(comment: Comment) {

        try {
            await MySQL.dBClient.query(`INSERT INTO comments(commentID,threadID,userID,creationDate,commentText,upVotes,downVotes) values(?,?,?,?,?,?,?)`,
                [comment.commentID, comment.threadID, comment.userID, comment.creationDate, comment.commentText, comment.upVotes, comment.downVotes]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async deleteComment(commentID: number) {

        try {
            await MySQL.dBClient.query(`DELETE FROM comment WHERE commentID=?`, [commentID]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async updateComment(commentID: number) {

        try {
            await MySQL.dBClient.query(`UPDATE comments SET commentText=? WHERE commentID=?`, [commentID]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getAllComments() {

        console.log("lol");
        try {
            return await MySQL.dBClient.query(`SELECT * FROM comments`);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getCommentsUserComment(threadID: number) {

    }

    static async getThreadComment(threadID: number) {

        try {
            return await MySQL.dBClient.query(`
                SELECT 
                    comments.commentID, 
                    comments.titel, 
                    comments.commentText, 
                    comments.upvotes, 
                    comments.downVotes, 
                    comments.views, 
                    comments.creationDate, 
                    comments.changes, 
                    users.username, 
                    users.email, 
                    users.registeredSince,
                    ranks.label,
                    ranks.hexcolor
                FROM comments 
                JOIN users ON userID=users.uuid 
                JOIN ranks ON users.rank=ranks.id 
                WHERE threadID=?        
            `, threadID);
        } catch (e) {
            console.log(e);
            return e;
        }

    }
}