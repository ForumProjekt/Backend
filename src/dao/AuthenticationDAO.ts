import { MySQL } from '../services/MySQL';
import { Logger } from '../services/Logger';
import { User } from '../models/User';
const uuidv4 = require('uuid/v4');
const bcrypt = require('bcrypt');

export class AuthenticationDAO {

    static async savePassword(_user: User): Promise<boolean> {
        let _return = false;
        await bcrypt.hash(_user.password, 10, function (err, hash) {
            if (hash) {
                Logger.debug('savePassword - generated hash: ' + hash);
                try {
                    MySQL.dBClient.query(`UPDATE users SET password = ? WHERE users.uuid = ?`, [hash, _user.uuid]);
                    Logger.debug('savePassword - updated password hash in database');
                    _return = true;
                } catch (sqlerr) {
                    Logger.sql('savePassword - f-ailed to update database: ' + sqlerr);
                    _return = false;
                }
            } else {
                Logger.error('savePassword - failed to generate hash: ' + err);
                _return = false;
            }
        });
        return _return;
    }

    static async validateLoginData(_user: User): Promise<boolean> {
        let _pwhash;
        try {
            const _query = await MySQL.dBClient.query(`SELECT password FROM users WHERE uuid = ?`, [_user.uuid]);
            _pwhash = _query[0].password;
        } catch (error) {
            Logger.sql(error);
        }
        Logger.debug('validateLoginData - Plain password entered: ' + _user.password);
        Logger.debug('validateLoginData - Password hash from DB: ' + _pwhash);

        let result = await bcrypt.compare(_user.password, _pwhash);
        return result;
    }

    static async getUUIDFromUser(_user: User): Promise<User> {
        try {
            const _query = await MySQL.dBClient.query(`SELECT uuid FROM users WHERE username = ?`, [_user.username]);
            _user.uuid = _query[0].uuid;
        } catch (sqlerr) {
            Logger.sql('getUUIDByName - Can\'t fetch UUID for given username: ' + sqlerr);
        }
        Logger.debug('getUUIDByName - UUID from DB:' + _user.uuid);
        return _user;
    }

    static async register(_user: User): Promise<boolean> {
        _user.uuid = uuidv4();
        let _return = false;

        try {
            await MySQL.dBClient.query(
                'INSERT INTO `users`(`uuid`, `username`, `password`, `email`, `firstname`, `lastname`, `birthdate`, `biography`, `location`, `homepage`) VALUES (?,?,?,?,?,?,?,?,?,?)',
                [_user.uuid, _user.username, 'NO_PASSWORD_SET', _user.email, _user.firstname, _user.lastname, _user.birthdate, _user.biography, _user.location, _user.homepage]
            );
            Logger.debug('register - created new user entry in db');
            await this.savePassword(_user);
            Logger.debug('register - inserted password hash for new user in db');
            _return = true;
        } catch (error) {
            Logger.sql('register - ' + error);
            _return = false;
        }

        return _return;
    }

}
