import { MySQL } from '../services/MySQL';
import { User } from '../models/User';
import { AuthenticationDAO } from './AuthenticationDAO';
import { Rank } from '../models/Rank';
import { Logger } from '../services/Logger';

export class RankDAO {

    static async addRank(rank: Rank) {
        try {
            await MySQL.dBClient.query(`INSERT INTO ranks (id, name, hexcolor) VALUES ?, ? ,?`, [rank.id, rank.name, rank.hexcolor]);
        } catch (e) {
            Logger.debug('[addRank] ' + e);
            return e;
        }
    }

    static async deleteRank(id: number) {

        try {
            await MySQL.dBClient.query(`DELETE FROM rabks WHERE ID=?`, [id]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getRanks() {

        try {
            return await MySQL.dBClient.query(`SELECT * FROM ranks`);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getRankByID(id: string) {

        try {
            return await MySQL.dBClient.query(`SELECT * FROM ranks WHERE id=?`, [id]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async patchRank(rank: Rank) {

        try {
            await MySQL.dBClient.query(`UPDATE ranks SET label = ?, hexcolor = ? WHERE id = ?`, [rank.name, rank.hexcolor, rank.id]);
        } catch (e) {
            console.log(e);
            return e;
        }
    }

    static async patchUserRank(rank: Rank, user: User) {

        try {
            await MySQL.dBClient.query(`UPDATE users SET rank=? WHERE UUID=?`, [rank.id, user.uuid]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

}