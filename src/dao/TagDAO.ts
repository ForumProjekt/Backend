import { MySQL } from '../services/MySQL';
import { Tag } from '../models/Tag';
import { Logger } from '../services/Logger';

export class TagDAO {

    static async addTag(tag: string) {

        try {
            await MySQL.dBClient.query(`INSERT INTO tags (titel) VALUES (?)`, [tag]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getTag(tag: string) {

        try {
            return await MySQL.dBClient.query(`SELECT * from tags WHERE titel = ?`, [tag]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async deleteTag(tagID: number) {

        try {
            await MySQL.dBClient.query(`DELETE FROM tags WHERE tagID=?`, [tagID]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async updateThreadTags(threadID: number, newTagIDs: number[], newTagLabels: string) {

        try {
            await MySQL.dBClient.query(`DELETE * FROM threads_tags WHERE threadID=?`, [threadID]);
            await this.addTagToThread(threadID, newTagLabels);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async addTagToThread(threadID: number, tagLabels: string) {

        // Tag String mit , getrennt in String Array umwandeln
        const tagLabelsArray: string[] = tagLabels.split(',');

        try {

            let correctedLabels: string[] = [];
            let existingLabelsInDB: string[] = [];
            let labelIDs: number[] = [];

            // Labels umformen
            for (let label of tagLabelsArray) {
                correctedLabels.push(await this.tagCorrection(label)); 2
            }

            // Vorhandensein in DB überprüfen
            for (let label of correctedLabels) {
                const sqlLabelExists = await MySQL.dBClient.query(`SELECT * FROM tags WHERE titel = ?`, [label]);
                if (sqlLabelExists.length !== 0) {
                    existingLabelsInDB.push(sqlLabelExists[0].titel);
                }
            }

            // Fehlende Label Array erzeugen
            const missingLabels = correctedLabels.filter(item => existingLabelsInDB.indexOf(item) < 0);
            if (missingLabels.length === 0) {
                Logger.debug('[addTagToThread] - Fehlende Tags in DB: KEINE');
            } else {
                Logger.debug('[addTagToThread] - Fehlende Tags in DB: ' + missingLabels);

            }

            // Fehlende Label anlegen
            for (let label of missingLabels) {
                await this.addTag(label);
            }

            // Tag ID Array
            for (let label of correctedLabels) {
                const sql = await MySQL.dBClient.query(`SELECT tagID FROM tags WHERE titel = ?`, [label]);
                Logger.debug('[addTagToThread] - Label/ID: ' + label + '/' + sql[0].tagID);
                labelIDs.push(sql[0].tagID);
            }
            Logger.debug('[addTagToThread] - Tag ID Array: ' + labelIDs);

            // Alle Tags aus Thread löschen
            await MySQL.dBClient.query(`DELETE FROM threads_tags WHERE threads_tags.threadID = ?`, threadID);

            // Tags zu Thread hinzufügen
            for (let id of labelIDs) {
                await MySQL.dBClient.query(`INSERT INTO threads_tags(tagID, threadID) VALUES (?,?)`, [id, threadID]);
            }

        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async deleteTagFromThread(tagID: number, threadID: number) {

        try {
            await MySQL.dBClient.query(`DELETE FROM threads_tags WHERE tagID=? AND threadID=?`, [tagID, threadID]);
        } catch (e) {
            console.log(e);
            return e;
        }
    }

    // Tag groß/kleinschreibung korrigieren
    static async tagCorrection(tag: string) {
        return await this.capitalizeFirstLetter((await this.lowerCaseAllWordsExceptFirstLetters(tag)));
    }

    static async capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    static async lowerCaseAllWordsExceptFirstLetters(string) {
        return string.replace(/\w\S*/g, function (word) {
            return word.charAt(0) + word.slice(1).toLowerCase();
        });
    }
}