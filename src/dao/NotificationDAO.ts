import { MySQL } from '../services/MySQL';
import { Notification } from '../models/Notification';

export class NotificationDAO {

    static async addNotification(notification: Notification) {
        console.log(notification.notificationText);
        try {
            await MySQL.dBClient.query(`INSERT INTO notifications(notificationText) values(?)`,
                [notification.notificationText]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async deleteNotification(notificationId: number) {

        try {
            await MySQL.dBClient.query(`DELETE FROM notifications WHERE alertid=?`, [notificationId]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getNotifications() {
        try {
            return await MySQL.dBClient.query(`SELECT * FROM notifications`);
        } catch (e) {
            console.log(e);
            return e;
        }

    }
}