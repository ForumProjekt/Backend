import { MySQL } from '../services/MySQL';
import { User } from '../models/User';
import { AuthenticationDAO } from './AuthenticationDAO';
import { Rank } from '../models/Rank';

export class UserDAO {

    static async deleteUser(uuid: number) {

        try {
            await MySQL.dBClient.query(`DELETE FROM users WHERE UUID=?`, [uuid]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getUsers() {

        try {
            return await MySQL.dBClient.query(`SELECT * FROM users`);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async updateUser(user: User) {

        try {
            console.log(user.uuid + '/' + user.password);
            // await AuthenticationDAO.savePassword(user);
            await MySQL.dBClient.query(`UPDATE users SET username=?, email=?, firstname=?, lastname=?, birthdate=?, biography=?, location=?, homepage=?, level=?, rank=? WHERE UUID=?`, [user.username, user.email, user.firstname, user.lastname, user.birthdate, user.biography, user.location, user.homepage, user.level, user.rank, user.uuid]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getUserByID(uuid: string) {

        try {
            return await MySQL.dBClient.query(`SELECT * FROM users WHERE uuid=?`, [uuid]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async patchUserRank(rank: Rank, user: User) {

        try {
            await MySQL.dBClient.query(`UPDATE users SET rank=? WHERE UUID=?`, [rank.id, user.uuid]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

}