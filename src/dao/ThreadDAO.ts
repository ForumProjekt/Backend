import { MySQL } from '../services/MySQL';
import { Thread } from '../models/Thread';
import { Logger } from '../services/Logger';

export class ThreadDAO {

    static async addThread(thread: Thread, tags: string) {

        try {

            // Insert Thread in DB
            await MySQL.dBClient.query(`INSERT INTO threads(titel,threadText,userID) values(?,?,?)`, [thread.titel, thread.threadText, thread.userID]);
            
        } catch (e) {

            Logger.error('[addThread] ' + e)
            return e;
        }

        try {

            // Get ThreadID from Database
            const sqlThreadID = await MySQL.dBClient.query(`SELECT threadID FROM threads WHERE userID = ? ORDER BY threadID DESC LIMIT 1`, thread.userID);
            Logger.debug('[addThread] Thread created with ID: ' + sqlThreadID[0].threadID);
            return sqlThreadID;

        } catch (e) {

            Logger.error('[addThread] get ThreadID error: ' + e);
            return e;

        }

    }

    static async deleteThread(threadID: number) {

        try {
            this.deleteTagToThread(threadID);
            await MySQL.dBClient.query(`DELETE FROM threads WHERE threadID=?`, [threadID]);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getThreads() {

        try {
            return await MySQL.dBClient.query(`SELECT * FROM threads`);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async updateThread(threadID: number, titel: string, threadText: string, threadTags: number[]) {

        try {
            this.deleteTagToThread(threadID);
            await MySQL.dBClient.query(`UPDATE threads SET titel=?, threadText=? WHERE threadID=?`, [titel, threadText, threadID]);
            this.addTagToThread(threadID, threadTags);
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async getThreadsbyID(threadID: number) {
        try {
            const query = await MySQL.dBClient.query(`
                SELECT 
                    threads.threadID, 
                    threads.titel, 
                    threads.threadText, 
                    threads.upvotes, 
                    threads.downVotes, 
                    threads.views, 
                    threads.creationDate, 
                    threads.userID,
                    threads.lastEdit, 
                    threads.changes, 
                    users.username, 
                    users.email,
                    users.registeredSince,
                    ranks.label,
                    ranks.hexcolor
                FROM threads 
                JOIN users ON userID=users.uuid 
                JOIN ranks ON users.rank=ranks.id 
                WHERE threadID=?        
            `, threadID);
            return query[0];
        } catch (e) {
            console.log(e);
            return e;
        }

    }

    static async addTagToThread(threadID, threadTags) {
        try {
            threadTags.forEach(tag => {
                MySQL.dBClient.query(`INSERT INTO tags_threads VALUES(?,?)`, [threadID, tag]);
            });
        } catch (e) {
            console.log(e);
            return e;
        }


    }

    static async deleteTagToThread(threadID) {

        try {
            await MySQL.dBClient.query(`DELETE FROM threads WHERE threadID=?`, [threadID]);
        } catch (e) {
            console.log(e);
            return e;
        }
    }

}