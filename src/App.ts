import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as fs from 'fs';

import HeroRouter from './routes/HeroRouter';
import ThreadRouter from './routes/ThreadRouter';
import CommentRouter from './routes/CommentRouter';
import AuthenticationRouter from './routes/AuthenticationRouter';
import TestRouter from './routes/TestRouter';
import UserRouter from './routes/UserRouter';
import TagRouter from './routes/TagRouter';
import SystemRouter from './routes/SystemRouter';
import RankRouter from './routes/RankRouter';
import NotificationRouter from './routes/NotificationRouter';
import BanRouter from './routes/BanRouter';
import { Logger } from './services/Logger';

const expressJwt = require('express-jwt');


// Creates and configures an ExpressJS web server.
class App {

  // ref to Express instance
  public express: express.Application;

  static testing = false;

  //Run configuration methods on the Express instance.
  constructor() {
    this.express = express();
    this.middleware();
    this.routes();
  }

  // Configure Express middleware.
  private middleware(): void {
    this.express.use(logger('dev'));
    this.express.use(cors());
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  // Configure API endpoints.
  private routes(): void {

    let router = express.Router();

    var RSA_PUBLIC_KEY = fs.readFileSync('./src/keys/pointless-JWT-public.key');

    const checkIfAuthenticated = expressJwt({
      secret: RSA_PUBLIC_KEY
    });

    this.express.use('/threads', ThreadRouter);
    this.express.use('/comments', CommentRouter);
    this.express.use('/heroes', HeroRouter);
    this.express.use('/auth', AuthenticationRouter);
    this.express.use('/users', UserRouter);
    this.express.use('/tags', TagRouter);
    this.express.use('/comments', CommentRouter);
    this.express.use('/system', SystemRouter);
    this.express.use('/ranks', RankRouter);
    this.express.use('/notifications', NotificationRouter);
    this.express.use('/bans', BanRouter);

    // Secured Routes
    // this.express.use('/whatever').get(checkIfAuthenticated, WhatEverRouter);
    this.express.route('/test').get(checkIfAuthenticated, TestRouter);

    // Custom Unauthorized Error Message without error stack
    this.express.use(function (err, req, res, next) {
      Logger.error(err);
      if (err.name === 'UnauthorizedError') {
        res.status(err.status).send(err.status + " - " + err.message);
        return;
      }
      next();
    })

    // Not Found Route
    this.express.use('*', function (req, res) {
      res.status(404).json({ 'statusCode': 404, 'message': 'Route not found', 'additionalMessage': 'visit our dev docs for help: https://docs.pointless.ga' });
    })

  }
}

let testRunning = false;

export default new App().express;
