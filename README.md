# RESTful API für Angular Frontend

## Build/Run

1. Install typescript package globaly via npm - `npm i -g typescript`
1. Install gulp and nodemon globally via npm - `npm i -g nodemon gulp`
1. Install projects dependencies - `npm i`
1. Compile TypeScript to JS with gulp, run gulp in Background - `gulp`
1. Open new Terminal and run the development server on port 3000 - `npm start`
1. Test - `npm test`

## Datenbank Download
Benötigte Datenbank der REST API zum Exportieren unter https://unbekannt3.eu/phpmyadmin  
Benutzer: `c4Projekt`, Passwort: `SZUT12345`  
Datenbank `c4<Production|Development>` öffnen, im Menü auf Exportieren klicken und als SQL Datei exportieren, lokale Datenbank in PHPMyAdmin öffnen und dort unter Importieren SQL Datei hochladen.

## Serververwaltung
WebGUI zur Verwaltung von Domain, Datenbanken, E-Mails unter https://host1.unbekannt3.eu:8080  
Benutzer: `projekt`, Passwort: `SZUT12345`
___
FTP Daten für Build Upload auf api.pointless.ga:  
Host: `host1.unbekannt3.eu`, Benutzer: `projektAPI`, Passwort `SZUT12345`