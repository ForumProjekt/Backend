#!/bin/bash
gulp scripts
cd dist/
ln -s ../src/backendConfig.json backendConfig.json
ln -s ../src/data.json data.json
cd ../
passenger start --app-type node --startup-file dist/index.js
exit 130